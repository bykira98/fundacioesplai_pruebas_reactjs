import React from "react";

import "./css/Lista.css"
class Lista extends React.Component
{
    render()
    {
        let i=0;
        let lis = this.props.datos.map(el => <li key={i++}>{el}</li>);
        return(
            <ul>
                {lis}
            </ul>
        );
    }
}

export default Lista;