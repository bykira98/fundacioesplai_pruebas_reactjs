import React from "react";

class SubTitulo extends React.Component {
    render() {
        return (
            <h2>{this.props.text.toUpperCase()}</h2>
        )
    }
}

export default SubTitulo;