import React from "react";

import Titulo from "./Titulo";
import SubTitulo from "./SubTitulo";
import Lista from "./Lista";
import Foto from "./Foto";

import "./css/App.css";



class App extends React.Component {
  render() {
    const Listado = ["un", "dos", "tres"];
    return (
      <>
        <Titulo>Welcome to React Parcel Micro App!</Titulo>
        <SubTitulo text="Pruebas con reactjs"></SubTitulo>
        <p>Hard to get more minimal than this React app.</p>
        <Lista datos={Listado} /> 
        <Foto ancho="220" alto="200"/>
      </>
    )
  }
};

export default App;